package BayesianRegret;

import java.lang.Math;

class Candidate
{
	//public double utilitySum = 0;
	private double fame = 0;
	
	Candidate()
	{
		fame = Math.random();
	}

	double utilitySum(Election election)
	{
		if(election.getUtilitySums().containsKey(this))
		{
			return election.getUtilitySums().get(this);
		}
		else
			return 0.0;

	}
}