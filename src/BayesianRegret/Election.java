package BayesianRegret;

import java.util.HashMap;

class Election
{
	private Candidate[] candidates;
    Candidate[] getCandidates() { return candidates; }

    /*
	public Candidate getCandidate(int index)
	{
		return candidates[index];
	}
	public int getNumCandidates()
	{
		return candidates.length;
	}
	*/

	private Voter[] voters;
    Voter[] getVoters() {return voters; }

    /*
	public Voter getVoter(int index) { return voters[index]; }
	public int getNumVoters() { return voters.length; }
	*/

	private HashMap<Candidate, Double> utilitySums;
    HashMap<Candidate, Double> getUtilitySums() { return utilitySums; }

    //public double getUtilitySum(Candidate key) { return utilitySums.get(key); }

	public Election(int numCandidates, int numVoters)
	{
        this.candidates = new Candidate[numCandidates];
        this.voters = new Voter[numVoters];
        this.utilitySums = new HashMap<>();

        int i = 0;
        for(i = 0; i < numVoters; i++)
        {
            voters[i] = new Voter(numCandidates);
        }

        for(i = 0; i < numCandidates; i++)
        {
            candidates[i] = new Candidate();
        }

        calculateUtilitySums(this.candidates, this.voters);


	    /* Unfortunately not legal code, because java requires this() to be called before anything else.
	    Candidate[] candidates = new Candidate[numCandidates];
        for(int i = 0; i < numCandidates; ++i)
        {
            candidates[i] = new Candidate();
        }

        Voter[] voters = new Voter[numVoters];
        for(int i = 0; i < numVoters; ++i)
        {
            voters[i] = new Voter(candidates);
        }

        this(candidates, voters);
        */
	}
	
	Election(Candidate[] candidates, Voter[] voters)
	{
		this.candidates = candidates;
		this.voters = voters;
        this.utilitySums = new HashMap<>();
		
		calculateUtilitySums(this.candidates, this.voters);
	}
	
	Candidate findBest()
	{
		Candidate BestCandidate = candidates[0];

        for (Candidate C : candidates) {
            //System.out.println("Checking candidate " + C + ": " + C.utilitySum);
            if (C.utilitySum(this) > BestCandidate.utilitySum(this))
            {
                BestCandidate = C;
            }
        }
		
		//System.out.println("Best found: " + BestCandidate + ": " + BestCandidate.utilitySum);
		
		return BestCandidate;
	}
	
	private void calculateUtilitySums(Candidate[] candidates, Voter[] voters)
	{
		double candidateSum;
		for(int i = 0; i < candidates.length; i++)
		{
			candidateSum = 0.0;
            for (Voter voter : voters) {
                candidateSum += voter.getUtilities()[i];
            }

			utilitySums.put(candidates[i], candidateSum);
		}
	}
}