package BayesianRegret;

abstract class ElectionMethod
{
	abstract Candidate findWinner(Election election);
}