package BayesianRegret;

class LinearScaler
{
	static double[] scale(double scaledMin, double scaledMax, double[] inArray)
	{
		// If the array contains nothing or a single element, I can't scale it.
		if(inArray.length <= 1)
			return inArray;
		
		double current = 0;
		double unscaledMin = inArray[0];
		double unscaledMax = unscaledMin;
		
		for(double value : inArray)
		{
			if(value < unscaledMin)
			{
				unscaledMin = value;
				continue;
			}
			
			if(value > unscaledMax)
			{
				unscaledMax = value;
			}
		}
		
		// If all of the arrays elements are the same, I can't scale it.
		if(unscaledMin == unscaledMax)
		{
			return inArray;
		}
		
		double scale_factor = (scaledMax - scaledMin) / (unscaledMax - unscaledMin);
		double offset = scaledMin - scale_factor * unscaledMin;
		
		double[] scaledArray = new double[inArray.length];
		
		//for(double value : inArray)
		int i = 0;
		for(i = 0; i < inArray.length; i++)
		{
			scaledArray[i] = scale_factor * inArray[i] + offset;
		}
		
		return scaledArray;
	}
}