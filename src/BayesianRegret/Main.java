package BayesianRegret;

public class Main
{
	public static void main(String[] args) {
        System.out.println("Start!");

        int numCands = 10;
        int numVoters = 500;
        int numElections = 1000000;

        double progressIncrements = 0.025;//0.0314159265;
        int progressTracker = (int)(numElections * progressIncrements);
        int lineBreakEvery = 10;

        double scoreRegret = 0.0;
        double abstainRegret = 0.0;

        Election election;
        Candidate[] candidates;
        Voter[] voters;
        Strategy SSS = new ScaledSincerity();
        ElectionMethod score = new ScoreAbstainMethod(100);
        ElectionMethod abstain = new ScoreAbstainMethod(0.035);

        Candidate Best;
        Candidate Winner;
        Candidate AbstainWinner;

        for (int i = 0; i < numElections; ++i)
        {
            candidates = new Candidate[numCands];
            for(int j = 0; j < candidates.length; ++j)
            {
                candidates[j] = new Candidate();
            }

            voters = new Voter[numVoters];
            for(int j = 0; j < voters.length; ++j)
            {
                voters[j] = new Voter(candidates, SSS, 0);
            }
            election = new Election(candidates, voters);

            Best = election.findBest();
            Winner = score.findWinner(election);
            scoreRegret += Best.utilitySum(election) - Winner.utilitySum(election);

            AbstainWinner = abstain.findWinner(election);

            abstainRegret += Best.utilitySum(election) - AbstainWinner.utilitySum(election);

            if(i % progressTracker == 0)
            {
                System.out.format("%5.2f%%... ", (i/(double)numElections)*100);
                if(i % (progressTracker * lineBreakEvery) == (progressTracker * lineBreakEvery) - progressTracker
                        || i == numElections-progressTracker)
                {
                    System.out.println();
                }
            }
        }
        System.out.println("\n100%\n");
        System.out.println("Bayesian Regret: " + (scoreRegret/numElections));
        System.out.println("Ignorant regret: " + (abstainRegret/numElections));
        System.out.println("Difference: " + ((scoreRegret/numElections)-(abstainRegret/numElections)));
	}
}