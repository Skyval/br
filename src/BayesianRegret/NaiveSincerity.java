package BayesianRegret;

class NaiveSincerity extends Strategy
{
	public double[] strategize(Voter voter)
	{
		return voter.getJudgement();
	}
}