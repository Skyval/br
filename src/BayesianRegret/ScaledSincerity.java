package BayesianRegret;

class ScaledSincerity extends Strategy
{
	public double[] strategize(Voter voter)
	{
		return LinearScaler.scale(0, 1, voter.getJudgement());
	}
}