package BayesianRegret;

import java.lang.Math;

class ScoreAbstainMethod extends ElectionMethod
{
    private double cutoff = 100;

    ScoreAbstainMethod(double cutoff)
    {
	    this.cutoff = cutoff;
    }

    public ScoreAbstainMethod() { this(100); }

	public Candidate findWinner(Election election)
	{
		Voter currentVoter;
		Candidate Winner = election.getCandidates()[0]; // Initialize winner
		double bestAvg = 0;		// Keep a record of current best average
		double newAvg = 0;		// Current candidate's average
		double newSum = 0;		// Working value for current candidate's utility sum before averaging
		int opinions = 0;		// Number of people who did have an opinion on a candidate.
		
		for(int i = 0; i < election.getCandidates().length; i++)
		{
			newSum = 0;
			opinions = 0;
			
			for(int j = 0; j < election.getVoters().length; j++)
			{
				currentVoter = election.getVoters()[j];

                if(Math.abs(currentVoter.getIgnorance()[i]) < this.cutoff)
				{
					newSum += Math.floor(currentVoter.scores[i]*10);
					opinions++;
				}// else { System.out.println("Found highly ignorant voter, ignorance was at: " + currentVoter.getIgnorance()[i]); }
			}
			
			newAvg = newSum/(double)opinions;
			
			if(newAvg > bestAvg)
			{
				Winner = election.getCandidates()[i];
				bestAvg = newAvg;
			}
		}

		return Winner;
	}
}