package BayesianRegret;/// import BayesianRegret.ElectionMethod;

/**
 * Created by Sky on 2016-08-20.
 */
public class ScoreMethod extends ElectionMethod {

    public Candidate findWinner(Election election)
    {
        Candidate Winner = election.getCandidates()[0]; // Initialize winner
        double bestSum = 0; // Keep a record of current best average
        double newSum;      // Working value for current candidate's utility sum before averaging

        for(int i = 0; i < election.getCandidates().length; i++)
        {
            newSum = 0;

            for(int j = 0; j < election.getVoters().length; j++)
            {
                newSum += Math.floor(election.getVoters()[j].scores[i]*10);
            }

            if(newSum > bestSum)
            {
                Winner = election.getCandidates()[i];
                bestSum = newSum;
            }
        }

        return Winner;
    }
}
