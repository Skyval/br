package BayesianRegret;

abstract class Strategy
{
	abstract double[] strategize(Voter voter);
}