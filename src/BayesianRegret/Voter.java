package BayesianRegret;

//import java.lang.Math;
import java.util.Random;


class Voter
{
	private double[] utilities; // The actually true utilities for each candidate for this voter.
	double[] getUtilities() { return utilities; }

	private double[] ignorance; // Sources if ignorance/inaccuracy. Effected by Candidate fame. Can make them think they are better or worse.
	double[] getIgnorance() { return ignorance; }

	private double[] judgement; // The scores the voter would give to each candidate if honest and forced. High enough ignorance may cause them to list "no opinion" if able.
	double[] getJudgement() {return judgement; }

	double[] scores;    // The scores the voter would give to each candidate if forced using a strategy. High enough ignorance may cause them to list "no opinion" if able.

	Voter(int numCandidates)
	{
		utilities = new double[numCandidates];

		for(int i = 0; i < numCandidates; i++)
		{
			utilities[i] = Math.random();
		}
	}

	Voter(Candidate[] candidates)
	{
	    this(candidates, new ScaledSincerity(), 0);
	}

	Voter(Candidate[] candidates, Strategy strategy)
	{
        this(candidates, strategy, 0);
    }

	Voter(Candidate[] candidates, Strategy strategy, double ignoranceAmplitude)
	{
	    Random rand = new Random();
	    int numCands = candidates.length;
	    utilities = new double[numCands];
        ignorance = new double[numCands];
        judgement = new double[numCands];
        scores    = new double[numCands];

        //System.out.println("IgnoranceAmplitude: " + ignoranceAmplitude);
        for(int i = 0; i < numCands; ++i)
        {
            utilities[i] = rand.nextGaussian();// Math.random();
            ignorance[i] = ignoranceAmplitude * (rand.nextGaussian()/*Math.random() * 2 - 1*/);
            judgement[i] = utilities[i] + ignorance[i];
//            System.out.println("judgement[i]: " + judgement[i]);
//            System.out.println("Utilities[i]: " + utilities[i]);
//            System.out.println("Ignorance[i]: " + ignorance[i]);
        }

        scores = strategy.strategize(this);
	}
}
